#include <stdio.h>
typedef struct
{
  char name[20];
  int age;
} Person;
void readData(Person *p);
Person findMiddleAge(Person *p);
int main()
{
  Person man[3], middle;

  readData(man);
  middle = findMiddleAge(man);
  printf("findMiddleAge(): %s %d\n", middle.name, middle.age);
  return 0;
}
void readData(Person *p)
{
  /* Write your program code here */
  int i;
  for (i = 0; i < 3; i++)
  {
    printf("Enter person then age %d:", i);
    scanf("%s", p[i].name);
    scanf("\n%d", &p[i].age);
  }
}
Person findMiddleAge(Person *p)
{
  int a = p[0].age, b = p[1].age, c = p[2].age;
  if (a > b && a < c)
  {
    return p[0];
  }
  else if (b > a && b < c)
  {
    return p[1];
  }
  else
    return p[2];
}
