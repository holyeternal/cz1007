#include <stdio.h>
//by Sky
#define SIZE 100
void compress2D(int data[SIZE][SIZE], int rowSize, int colSize);
int main()
{
    int data[SIZE][SIZE];
    int i, j;
    int rowSize, colSize;
    printf("Enter the array size (rowSize, colSize): \n");
    scanf("%d %d", &rowSize, &colSize);
    printf("Enter the matrix (%dx%d): \n", rowSize, colSize);
    for (i = 0; i < rowSize; i++)
        for (j = 0; j < colSize; j++)
            scanf("%d", &data[i][j]);
    printf("compress2D(): \n");
    compress2D(data, rowSize, colSize);
    return 0;
}


void compress2D(int data[SIZE][SIZE], int rowSize, int colSize) {
    int numCount;
    for (int rowCount = 0; rowCount < rowSize; rowCount++){
        numCount = 1;
        for (int colCount = 1; colCount < colSize ; colCount++){
            if(data [rowCount][colCount - 1] == data[rowCount][colCount]) {
                numCount++;
            }
            else{
                printf("%d %d ",data[rowCount][colCount - 1], numCount);
                numCount = 1;
            }
        }
        printf("%d %d \n",data[rowCount][colSize -1], numCount);
    }       
}