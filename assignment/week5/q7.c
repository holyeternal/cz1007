#include <stdio.h>
#include <string.h>
#define SIZE 10
void findMinMaxStr(char word[][40], char *first, char *last, int size);
int main()
{
    char word[SIZE][40];

    char first[40], last[40];
    int i, size;

    printf("Enter size: \n");
    scanf("%d", &size);
    printf("Enter %d words: \n", size);
    for (i = 0; i < size; i++)
        scanf("%s", word[i]);
    findMinMaxStr(word, first, last, size);
    printf("First word = %s, Last word = %s\n", first, last);
    return 0;
}
void findMinMaxStr(char word[][40], char *first, char *last, int size)
{
    int _first = 0, _last = 0;

    for (int wordindex = 0; wordindex < size; wordindex++) {
        
    // king of the hill (top)
        for (int str1index = 0; str1index < 40; str1index++)
            if (word[_first][str1index] > word[wordindex][str1index]) {
                _first = wordindex;
                break;
            } else if (word[_first][str1index] < word[wordindex][str1index]){
                break;
            }

    // king of the hill (bot)
        for (int str1index = 0; str1index < 40; str1index++)
            if (word[_last][str1index] < word[wordindex][str1index]) {
                _last = wordindex;
                break;
            }else if (word[_last][str1index] > word[wordindex][str1index]) {
                break;
            }
    

    }

    //first & last (push one by one in) 
    for (int i = 0; i < 40; i++) *(first+i) = word[_first][i];
    for (int i = 0; i < 40; i++) *(last+i) = word[_last][i];
        
}