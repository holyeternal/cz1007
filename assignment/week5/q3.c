#include <stdio.h>
#include <string.h>
int longWordLength(char *s);
int main()
{
    char str[80], *p;

    printf("Enter a string: \n");
    fgets(str, 80, stdin);
    if (p == strchr(str, '\n'))
        *p = '\0';
    printf("longWordLength(): %d\n", longWordLength(str));
    return 0;
}
int longWordLength(char *s)
{
    int longword = 0, buffer = 0;
    for (int i = 0; i < strlen(s); i++)
    {
        if (longword < buffer)
            longword = buffer;
        if (s[i] == ' ')
            buffer = 0;
        else
            buffer++;
    }
    return longword;
}



/* Alternative Form by Sky
int longWordLength(char *s)
{
    int longest = 0, count = 0;
    for (int i = 0; i < strlen(s); i++){
        count ++;
        if (s[i] == ' ' || s[i] =='\n') {
            if(count > longest) longest = count;
            count = 0;
        }
    }
    return longest;
}

#include <stdio.h>
#include <string.h>
int longWordLength(char *s);
int main()
{
    char str[80], *p;

    printf("Enter a string: \n");
    fgets(str, 80, stdin);
    if (p == strchr(str, '\n'))
        *p = '\0';
    printf("longWordLength(): %d\n", longWordLength(str));
    return 0;
}
int longWordLength(char *s)
{
    int longword = 0, buffer = 0;
    for (int i = 0; i < strlen(s); i++)
    {
        if (longword < buffer)
            longword = buffer;
        if (s[i] == ' ')
            buffer = 0;
        else
            buffer++;
    }
    return longword;
}

int longWordlength(char *s){
    int i = 0, count = 0, largest = 0;
    while(s[i] != '\0'){
        if (isalnum(s[i])) count ++;
        else count = 0; 
        if (largest < count) largest = count;
    }
    return largest;
}

*/

//zhenwei - all the answers not really hitting the test cases for some reason, space inclusion/exclusion,punctuation etc, question is not clear
/* alternate form matches 3/4 of test case, not sure if punctuation needs to be counted */
/*int longWordLength(char *s)
{
 int length = strlen(s);
 int wordLength =0;
 int max =0;

 for(int i =0; i <length; i++)
 {

    wordLength++;
    if(wordLength>max)
    {
        max = wordLength-1;
    }
    if(s[i]==' ')
    {
        if(wordLength>max)
        {
            max = wordLength-1;
        }
        wordLength =0;
    }
 }

 return max;

} */
