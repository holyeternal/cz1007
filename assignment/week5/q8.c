#include <stdio.h>
#include <string.h>
void maxCharToFront(char *str);
int main()
{
 char str[80], *p;

 printf("Enter a string: \n");
 fgets(str, 80, stdin);
 if (p=strchr(str,'\n')) *p = '\0';
 printf("maxCharToFront(): "); 
 maxCharToFront(str);
 puts(str);
 return 0;
}



void maxCharToFront(char *str)
{
    char c = 'a';
    int index;
    for (int i =0;i<strlen(str);i++)
    {
        if (str[i] > c){
            index=i;
            c = str[i];
        }
    }
    //printf("%c %c\n",str[index],c);
    //push other string back
    for(int i=index; i>=0;i--){
        str[i] = str[i-1];  
        //puts(str);
    }
    str[0] =c;
} 


//alternative Solution - By Sky
/*
void maxCharToFront(char *str)
{

    // find index of maxChar
    int index = 0;
    for (int i =0; i < strlen(str) ;i++)
        if (str[i] > str[index]) index = i;

    // shift chr from 0 to index(maxChar) to right by one & put MaxChar in index 0
    char temp = str[index];
    for (int i = index ; i > 0; i-- )
        str[i] = str[i - 1];
    str[0] = temp;
    
} 
*/