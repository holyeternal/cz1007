#include <stdio.h>
int rCountEvenDigits1(int num);
void rCountEvenDigits2(int num, int *result);
int main()
{
    int number, result;

    printf("Enter the number: \n");
    scanf("%d", &number);
    printf("rCountEvenDigits1(): %d\n", rCountEvenDigits1(number));
    rCountEvenDigits2(number, &result);
    printf("rCountEvenDigits2(): %d\n", result);
    return 0;
}
int rCountEvenDigits1(int num)
{
    int count = 0;
    if (num < 10)
        return (num % 2 == 0) ? 1 : 0;
    else
    {
        if (num % 2 == 0)
            count++;
        return count + rCountEvenDigits1(num / 10);
    }
}
void rCountEvenDigits2(int num, int *result)
{
    if (num < 10)
        *result = (num % 2 == 0) ? 1 : 0;
    else{
        rCountEvenDigits2(num/10,result);
        if(num % 2 == 0)
            *result += 1;
    }
}