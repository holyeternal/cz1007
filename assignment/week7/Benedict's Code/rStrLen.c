#include <stdio.h>

int rStrLen(char *s);

int main()
{
    char str[80];

    printf("Enter the string: \n");
    gets(str);
    printf("rStrLen(): %d\n", rStrLen(str));

    return 0;
}

int rStrLen(char *s)
{
    int length = 0;

    if(*s != '\0')
    {
        length++;
        length += rStrLen(++s);
    }
    return length;
}
